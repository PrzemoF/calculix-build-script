#!/bin/bash
DEPENDENCIES="tar make bzip2 gzip liblapack-dev libarpack2 libarpack2-dev libblas3 libblas-dev gfortran"

function check_pkg() {
        if pkgstat=$(dpkg-query -W -f '${Status},${Package} ${Version}\n' "$1" 2>/dev/null) && [[ $pkgstat = "install ok installed,"* ]]; then
                printf "%s\n" "${pkgstat#*,}";
        else
                printf "%s not installed\n" "$1";
		return 1;
        fi
}

#Check dependencies
for package in $DEPENDENCIES; do
        echo -n "Checking is $package is installed......."
        check_pkg $package ; if [ $? -ne 0 ]; then echo "$package is not installed. If you're sure you have it please remove it from DEPENDENCIES. Enabling Universe repo might be required";  exit 1; fi
done

#Build CalculiX
. build_ccx
