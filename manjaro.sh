#!/usr/bin/bash
PACKAGE_TOOL="pacman -Q"
DEPENDENCIES="tar patch make bzip2 gzip lapack arpack blas gcc-fortran"

#Check dependencies
for package in $DEPENDENCIES; do
	echo -n "Checking is $package is installed......."
	$PACKAGE_TOOL $package ; if [ $? -ne 0 ]; then echo "$PACKAGE_TOOL shows $package is not installed. If you're sure you have it please remove it from DEPENDENCIES";  exit 1; fi
done

#Build CalculiX
. build_ccx
