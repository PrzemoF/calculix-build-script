Start fedora.sh or ubuntu.sh. It will produce CalculiX binary. It can be copied to some other location and used with FreeCAD. There is no need to keep the build directory.

13th may 2020 tested with ccx 2.16:

* centos 7 - OK, see (1)

* debian 10.4 - OK

* fedora 30 - OK
* fedora 31 - OK
* fedora 32 - OK

* manjaro 20.0.1 - OK

* mint 19.3 - OK

* ubuntu 18.04 - OK, see (2)
* ubuntu 20.04 - OK, see (2)

(1) epel repo is needed. Enable with "yum install epel-release"
(2) universe repo is needed. Enable with "add-apt-repository universe"
